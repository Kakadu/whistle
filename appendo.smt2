(set-logic UF)
(set-option :produce-models true) 

(declare-sort T 0)
(declare-fun appendo (T T T) Bool)
(declare-fun cons (T T) T)
(declare-fun nil () T)

(assert (forall ((y T)) (appendo nil y y )))
(assert (forall ((x T) (y T) (z T) (w T)) (=> (appendo x y z) (appendo (cons w x) y (cons w z)))))

;(assert (forall ((w T) (y T)) (=> (appendo (cons w nil) y y) false)))
;(check-sat)
;(get-model)


(assert (forall ((v T) (w T) (y T)) (=> (appendo y (cons w (cons v nil)) y) false)))
(check-sat)
(get-model)



