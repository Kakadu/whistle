(set-logic UF)
(set-option :produce-models true)

(declare-sort T 0)
(declare-fun odd  (T) Bool)
(declare-fun even (T) Bool)

(declare-fun succ (T) T)
(declare-fun zero ()  T)

(assert (even zero))

(assert (odd (succ zero)))

(assert (forall ((y T))
  (=> (odd y) (even (succ y)))))

(assert (forall ((y T))
  (=> (even y) (odd (succ y)))))

; TODO: maybe implement this without mutual recursion
; using double step in each function


;(assert (forall ((x T)) (=> (and (odd x) (even x)) false)))
;(assert (exists ((x T)) (and (odd x) (even x)) ))

(assert (forall ((x T)) (=> (and (even x) (even x)) false)))   ; it is unsat as expected
(check-sat)
(get-model)
