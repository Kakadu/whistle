(set-logic UF)
(set-option :produce-models true) 

(declare-sort T 0)
(declare-fun addo (T T T) Bool)
(declare-fun succ (T) T)
(declare-fun zero () T)



;(assert (forall ((y T)) 
;  (addo zero y y)))
;(assert (forall ((x T) (y T) (z T) (w T)) 
;  (=> (addo x (succ y) z) (addo (succ x) y z))))



(assert (forall ((y T)) 
  (addo zero y y)))
(assert (forall ((x T) (y T) (z T) (w T)) 
  (=> (addo (succ x) y z) (addo x (succ y) z))))



(assert (forall ((x T)) (=> (addo x (succ zero) x) false)))
(check-sat)
(get-model)



