; Use cvc4 --finite-model-find filename.smt2 to execute
; https://cvc4.cs.stanford.edu/downloads/builds/x86_64-linux-opt/
(set-logic UF)
(set-option :produce-models true)

(declare-sort Num 0)

(declare-fun succ (Num) Num)
(declare-fun zero ()    Num)

(declare-sort Typ 0)

(declare-fun tvar (Num) Typ)
(declare-fun arrow (Typ Typ) Typ)

(declare-sort Env 0)

(declare-fun nil () Env)
(declare-fun cons (Num Typ Env) Env)

(declare-sort Expr 0)

(declare-fun evar (Num) Expr)
(declare-fun eabs (Num Expr) Expr)
(declare-fun eapp (Expr Expr) Expr)

(declare-fun lookupo (Env Num Typ) Bool)

;(assert (forall ((a Num) (b Typ)) (=> (lookupo nil a b) false)))
(assert (forall ((a Num) (t Typ) (x Env)) (lookupo (cons a t x) a t)))
(assert (forall ((a Num) (b Num) (t1 Typ) (t2 Typ) (x Env))
  (=> (and (not (= a b)) (lookupo x a t1))
      (lookupo (cons b t2 x) a t1))))

(declare-fun typo (Env Expr Typ) Bool)

(assert (forall ((env Env) (x Num) (t Typ))
  (=> (lookupo env x t)
      (typo env (evar x) t))))

(assert (forall ((env Env) (x Num) (t0 Typ) (t1 Typ) (b Expr))
  (=> (typo (cons x t0 env) b t1)
      (typo env (eabs x b) (arrow t0 t1)))))

(assert (forall ((env Env) (t0 Typ) (t Typ) (a Expr) (b Expr))
  (=> (and (typo env a (arrow t0 t)) (typo env b t0))
      (typo env (eapp a b) t))))



; TODO: maybe implement this without mutual recursion
; using double step in each function




(assert (forall ((e Expr) (x Num) (y Num)) (=> (and (= x y) (typo nil e (arrow (tvar x) (tvar y)))) false)))
;(assert (forall ((x T)) (=> (and (even x) (even x)) false)))   ; it is unsat as expected
(check-sat)
(get-model)
